package com.example.server;

import java.util.ArrayList;
import java.util.List;

import com.example.client.Person;
import com.example.client.PersonService;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;

public class PersonServiceImpl extends RemoteServiceServlet implements PersonService{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List<Person> people = new ArrayList<>();
	
	@Override
	public void add(Person person) {
		people.add(person);
		
	}

	@Override
	public List<Person> getAll() {
		return people;
	}

}
