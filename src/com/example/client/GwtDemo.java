package com.example.client;

import java.util.List;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.view.client.ListDataProvider;
import com.sun.media.sound.DataPusher;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class GwtDemo implements EntryPoint {
	
	private PersonServiceAsync personService = GWT.create(PersonService.class); 
	
	public void onModuleLoad() {
		

		HorizontalPanel mainHP = new HorizontalPanel();
		
		
		final TextBox firstNameTB = new TextBox();
		final TextBox lastNameTB = new TextBox();
		final TextBox emailTB = new TextBox();
		final TextBox yoBTB = new TextBox();
		
		CellTable<Person> personTable = new CellTable<Person>();
		final ListDataProvider<Person> dataProvider = new ListDataProvider<Person>();
		dataProvider.addDataDisplay(personTable);
		
		Grid form = new Grid(4,2);
		form.setWidget(0,0, new Label("first name"));
		form.setWidget(0,1,firstNameTB);
		form.setWidget(1,0,new Label("last name"));
		form.setWidget(1,1,lastNameTB);
		form.setWidget(2,0,new Label("email"));
		form.setWidget(2,1,emailTB);
		form.setWidget(3,0,new Label("year of birth"));
		form.setWidget(3,1,yoBTB);
		
		final Button addBtn = new Button("add");
		addBtn.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				personService.add(new Person(firstNameTB.getText(), lastNameTB.getText(), emailTB.getText(), Integer.parseInt(yoBTB.getText())), new AsyncCallback<Void>() {
					
					@Override
					public void onSuccess(Void result) {
						// TODO Auto-generated method stub
						
					}
					
					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						
					}
				});
				dataProvider.getList().clear();
				personService.getAll(new AsyncCallback<List<Person>>() {
					
					@Override
					public void onSuccess(List<Person> result) {
						dataProvider.getList().addAll(result);
						
					}
					
					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						
					}
				});
			}
		});
		
		
		TextColumn<Person> firstNameColumn = new TextColumn<Person>() {

			@Override
			public String getValue(Person object) {
				return object.getFirstName();
			}
			
		
		};
		
		TextColumn<Person> lastNameColumn = new TextColumn<Person>() {
			
			@Override
			public String getValue(Person object) {
				return object.getLastName();
			}
		};
		
		TextColumn<Person> emailColumn = new TextColumn<Person>() {
			
			@Override
			public String getValue(Person object) {
				return object.getEmail();
			}
		};
		
		TextColumn<Person> yoBColumn = new TextColumn<Person>() {
			
			@Override
			public String getValue(Person object) {
				return object.getYearOfBirth().toString();
			}
		};
		personTable.addColumn(firstNameColumn, "first name");
		personTable.addColumn(lastNameColumn, "last name");
		personTable.addColumn(emailColumn, "email");
		personTable.addColumn(yoBColumn, "year of birth");
		mainHP.add(personTable);
		mainHP.add(form);
		mainHP.add(addBtn);

		RootPanel.get("main").add(mainHP);

	}
}
