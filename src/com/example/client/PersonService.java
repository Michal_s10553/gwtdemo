package com.example.client;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("calc")
public interface PersonService extends RemoteService {
	
	public void add(Person person);
	
	public List<Person> getAll();
}
