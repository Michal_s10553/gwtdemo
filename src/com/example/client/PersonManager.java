package com.example.client;

import java.util.ArrayList;
import java.util.List;

public class PersonManager {

	private List<Person> people;

	public PersonManager() {
		super();
		people = new ArrayList<Person>();
	}
	
	public void add(Person person) {
		people.add(person);
	}
	
	public List<Person> getAll() {
		return people;
	}
	
}
