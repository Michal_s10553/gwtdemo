package com.example.client;

import java.util.List;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface PersonServiceAsync {

	void add(Person person, AsyncCallback<Void> callback);

	void getAll(AsyncCallback<List<Person>> callback);

}
